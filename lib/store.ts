import { combineReducers,configureStore } from '@reduxjs/toolkit'
import categoriesReducer from './features/categories/categoriesSlice';
import detailCategoriesSlice from './features/detail-categories/detailCategoriesSlice';

const rootReducer = combineReducers({
  categories: categoriesReducer,
  detailCategories: detailCategoriesSlice
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
  getDefaultMiddleware({
    serializableCheck: false,
  }),
 });


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch