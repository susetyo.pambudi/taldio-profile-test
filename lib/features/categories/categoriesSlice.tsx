"use client";

import { createSlice } from "@reduxjs/toolkit";
import {
  FaGraduationCap,
  FaBriefcase,
  FaAward,
  FaHeadSideVirus,
  FaFileLines,
  FaBookOpen,
  FaSuitcase,
} from "react-icons/fa6";
import { IoChatbubbleEllipses } from "react-icons/io5";

const initialState = {
  data: [
    {
      label: "Education",
      isActive: false,
      Icon: <FaGraduationCap size={23} />,
      id: "education",
    },
    {
      label: "Work Experience",
      isActive: false,
      Icon: <FaBriefcase size={23} />,
      id: "workExperience",
    },
    {
      label: "Assessments",
      isActive: true,
      Icon: <FaHeadSideVirus size={23} />,
      id: "assessments",
    },
    {
      label: "Resume and Portofolio",
      isActive: false,
      Icon: <FaFileLines size={23} />,
      id: "resumeAndPortofolio",
    },
    {
      label: "Achievements & Certification",
      isActive: false,
      Icon: <FaAward size={23} />,
      id: "achievementsAndCertification",
    },
    {
      label: "Language",
      isActive: false,
      Icon: <IoChatbubbleEllipses size={23} />,
      id: "language",
    },
    {
      label: "Job Preferences",
      isActive: false,
      Icon: <FaBookOpen size={23} />,
      id: "jobPreferences",
    },
    {
      label: "Additional Information",
      isActive: false,
      Icon: <FaSuitcase size={23} />,
      id: "additionalInformation",
    },
  ],
};

export const categoriesSlice = createSlice({
  name: "categories",
  initialState,
  reducers: {
    clickMenuSidebar: (state, action) => {
      state.data = action.payload;
    },
  },
});

export const { clickMenuSidebar } = categoriesSlice.actions;

export default categoriesSlice.reducer;
