"use client";

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [
    {
      skills: "Javascript",
      level: "Basic",
      date: new Date(),
      score: 80,
      dueDays: 30,
      isChecked: true,
      id: 1,
    },
    {
      skills: "php",
      level: "Advance",
      date: new Date(),
      score: 50,
      dueDays: 0,
      isChecked: true,
      id: 2,
    },
  ],
};

export const createDetailCategoriesSlice = createSlice({
  name: "detailCategories",
  initialState,
  reducers: {
    onDeleteItem: (state) => {
      state.items = state.items.filter((item) => item.isChecked);
    },
    onChangeItem: (state, action) => {
      state.items = action.payload;
    },
    onChangeItemAll: (state, action) => {
      const { isCheckedAll } = action.payload;
      state.items = state.items.map((item) => ({
        ...item,
        isChecked: isCheckedAll,
      }));
    },
  },
});

export const { onDeleteItem, onChangeItem, onChangeItemAll } =
  createDetailCategoriesSlice.actions;

export default createDetailCategoriesSlice.reducer;
