"use client";
import { Provider } from "react-redux";
import { store } from "./store";
import { ReactNode } from "react";

type TProvider = {
  children: ReactNode;
};

export function Providers({ children }: TProvider) {
  return <Provider store={store}>{children}</Provider>;
}
