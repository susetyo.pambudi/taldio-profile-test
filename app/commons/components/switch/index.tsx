import { Switch as SwitchMaterialUI } from "@mui/material";
import { styled } from "@mui/material/styles";

const CustomSwitch = styled(SwitchMaterialUI)(() => ({
  "& .MuiSwitch-thumb": {
    backgroundColor: "#ed6c02",
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#aab4be",
    borderRadius: 20 / 2,
  },
}));

export default CustomSwitch;
