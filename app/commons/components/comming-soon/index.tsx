import Card from "../card";

const CommingSoon = () => {
  return (
    <Card customClass="p-4 border-[#e6e6e6] border-2 flex justify-center items-center w-full">
      <div className="text-primary text-xl">Comming Soon</div>
    </Card>
  );
};

export default CommingSoon;
