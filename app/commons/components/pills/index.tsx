import { TPills } from "./types";
import Chip from "@mui/material/Chip";
import { styled } from "@mui/material/styles";

const CustomChip = styled(Chip)(() => ({
  "&.MuiChip-root": {
    color: "white",
    fontSize: 12,
  },
}));

const Pills = ({ color, text }: TPills) => {
  return (
    <CustomChip
      className={`bg-gradient-to-r ${color} text-white`}
      label={text}
      size="small"
    />
  );
};

export default Pills;
