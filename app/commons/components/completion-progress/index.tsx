const CompletionProgress = () => {
  const dummyPercent = 50;

  return (
    <div className="bg-[#faf0dc] border border-[#d76437] border-1 p-4 rounded-md mb-4">
      <div className="text-sm md:text-base font-extrabold mb-2">
        Profile Completion Progress
      </div>
      <div className="flex justify-between items-center gap-4">
        <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
          <div
            className="bg-[#d76437] h-2.5 rounded-full"
            style={{ width: `${dummyPercent}%` }}
          ></div>
        </div>
        <div className="text-xs font-extrabold">{dummyPercent}%</div>
      </div>
    </div>
  );
};

export default CompletionProgress;
