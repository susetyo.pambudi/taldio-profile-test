type TText = {
  label: string
  text: string
  customClass?: string
}

const Text = ({ label, text, customClass }: TText) => {
  return (
    <div className={customClass ? customClass : ''}>
      <div className="font-bold text-sm">{label}</div>
      <div className="text-sm text-[#888888]">{text}</div>
    </div>
  )
}

export default Text
