"use client";
import { Button as ButtonMaterialUI } from "@mui/material";
import { styled } from "@mui/material/styles";

type TButton = {
  text?: string;
  size?: "large" | "medium" | "small";
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  customClass?: string;
};

const CustomButton = styled(ButtonMaterialUI)(() => ({
  "&.MuiButton-outlinedPrimary": {
    borderColor: "#d76437",
    color: "#d76437",
    textTransform: "none",
  },
}));

const Button = ({ text, size = "small", onClick, customClass }: TButton) => {
  return (
    <CustomButton
      onClick={onClick}
      variant="outlined"
      className={customClass}
      disableRipple
      size={size}
    >
      {text}
    </CustomButton>
  );
};

export default Button;
