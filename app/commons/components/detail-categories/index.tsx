"use client";
import { useSelector } from "react-redux";
import { RootState } from "@/lib/store";
import Assesment from "@/app/commons/components/detail-categories/component/assessment/Assessment";
import CommingSoon from "../comming-soon";

const DetailCategories = () => {
  const categories = useSelector((state: RootState) => state.categories).data;
  const findId = categories.find((category) => category.isActive);
  const activeId = findId ? findId.id : "";

  if (activeId === "assessments") {
    return <Assesment />;
  }

  return <CommingSoon />;
};

export default DetailCategories;
