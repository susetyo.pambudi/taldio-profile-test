export type TTab = {
  tabId: string,
  isActive: boolean,
  label: string,
}

export type TTabs = {
  onClick: Function;
  tab: TTab,
  classNameObject: {
    classNameTechnicalSkills: string,
    classNamePersonality: string
  }
};

export type TTechnicalSkillItem ={
  skills: string,
  level: string,
  date: Date
  score: number
  isHaveWarning: boolean
  days: number
  pills: {
    label:string 
    size: 'small' | 'medium' 
    color: 'info' | 'error'
  },
  onChangeSwitch:Function,
  id:number,
  checked:boolean
}

export type TChangeSwitch = {
  isChecked: boolean;
  id: number;
};

export type TDelete = {
  id: number;
};