import { useState, useMemo } from "react";

const useAssesment = () => {
  const [tabs, setTabs] = useState([
    {
      tabId: "technicalSkills",
      isActive: true,
      label: "Technical Skills",
    },
    {
      tabId: "personality",
      isActive: false,
      label: "Personality",
    },
  ]);

  const tab = useMemo(() => {
    const findByTabId = tabs.find((tab) => tab.isActive);
    return findByTabId
      ? findByTabId
      : {
          tabId: "",
          isActive: false,
          label: "",
        };
  }, [tabs]);

  const onClickTab = (tabId: string) => {
    setTabs((prev) => {
      return prev.map((item) => ({
        ...item,
        isActive: item.tabId === tabId,
      }));
    });
  };

  const functionGenerateClassTabs = useMemo(() => {
    const defaultClassName =
      "py-2 flex justify-center items-center border border-b-2 border-t-0 border-l-0 border-r-0 w-[50%]";

    if (tab.tabId === "technicalSkills") {
      return {
        classNameTechnicalSkills:
          defaultClassName + " text-primary border-primary font-bold",
        classNamePersonality:
          defaultClassName + " text-secondary border-secondary",
      };
    }

    return {
      classNameTechnicalSkills:
        defaultClassName + " text-secondary border-secondary",
      classNamePersonality:
        defaultClassName + " text-primary border-primary font-bold",
    };
  }, [tab]);

  return {
    tabs,
    tab,
    onClickTab,
    functionGenerateClassTabs,
  };
};

export default useAssesment;
