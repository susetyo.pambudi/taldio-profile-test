import { useSelector, useDispatch } from "react-redux";
import { RootState, AppDispatch } from "@/lib/store";
import {
  onDeleteItem,
  onChangeItem,
  onChangeItemAll,
} from "@/lib/features/detail-categories/detailCategoriesSlice";
import { TChangeSwitch } from "../types";

const useTechnicalSkills = () => {
  const dispatch = useDispatch<AppDispatch>();
  const detailCategories = useSelector(
    (state: RootState) => state.detailCategories
  )?.items;

  const onChangeSwitch = ({ id, isChecked }: TChangeSwitch) => {
    const newDetailCategories = detailCategories.map((detailCategory) => {
      if (detailCategory.id === id) {
        return {
          ...detailCategory,
          isChecked,
        };
      }

      return detailCategory;
    });
    dispatch(onChangeItem(newDetailCategories));
  };

  const onDelete = () => {
    dispatch(onDeleteItem());
  };

  const onCheckAll = ({ isChecked }: { isChecked: boolean }) => {
    dispatch(onChangeItemAll({ isCheckedAll: isChecked }));
  };

  return {
    technicalSkills: detailCategories,
    onChangeSwitch,
    onDelete,
    onCheckAll,
  };
};

export default useTechnicalSkills;
