import { TTabs } from "../types";

const Tabs = ({ onClick, classNameObject }: TTabs) => {
  return (
    <div className="flex w-full">
      <div
        role="button"
        onClick={() => onClick("technicalSkills")}
        className={classNameObject.classNameTechnicalSkills}
      >
        Technical Skills
      </div>
      <div
        role="button"
        onClick={() => onClick("personality")}
        className={classNameObject.classNamePersonality}
      >
        Personality
      </div>
    </div>
  );
};

export default Tabs;
