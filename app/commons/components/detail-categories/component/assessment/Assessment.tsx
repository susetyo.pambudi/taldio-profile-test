import Tabs from "@/app/commons/components/detail-categories/component/assessment/Tabs";
import TechnicalSkills from "@/app/commons/components/detail-categories/component/assessment/TechnicalSkills";
import Personality from "@/app/commons/components/detail-categories/component/assessment/Personality";
import useAssesment from "../hooks/useAssesment";
import Card from "../../../card";

const Assessment = () => {
  const { tab, onClickTab, functionGenerateClassTabs } = useAssesment();

  return (
    <Card customClass="max-h-[400px] w-full border-[#faf0e6] border-[2px]">
      <Tabs
        onClick={onClickTab}
        tab={tab}
        classNameObject={functionGenerateClassTabs}
      />
      {tab.tabId === "technicalSkills" && <TechnicalSkills />}
      {tab.tabId === "personality" && <Personality />}
    </Card>
  );
};

export default Assessment;
