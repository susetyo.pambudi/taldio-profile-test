import TechnicalSkillItem from "./TechnicalSkillItem";
import Button from "../../../button";
import CustomSwitch from "../../../switch";
import DeleteIcon from "@mui/icons-material/Delete";
import { styled } from "@mui/material/styles";
import useScreenDetector from "@/app/commons/hooks/useScreenDetector";
import useTechnicalSkills from "../hooks/useTechnicalSkills";

const CustomDeleteIcon = styled(DeleteIcon)(() => ({
  "&.MuiSvgIcon-root": {
    color: "#d76437",
  },
}));

const TechnicalSkills = () => {
  const { technicalSkills, onChangeSwitch, onDelete, onCheckAll } =
    useTechnicalSkills();
  const { isMobile, isTablet } = useScreenDetector();

  return (
    <div className="mt-4 mx-4">
      <div className="flex w-full justify-between items-center mb-6">
        <div className="text-sm font-bold">Technical Skills</div>
        <Button
          onClick={() => alert("edit item")}
          text="Edit"
          size="medium"
          customClass="w-10"
        />
      </div>
      <div className={`overflow-y-scroll h-56`}>
        {technicalSkills?.map((technicalSkill) => (
          <TechnicalSkillItem
            key={`technicalSkill-${technicalSkill.id}`}
            skills={technicalSkill.skills}
            score={technicalSkill.score}
            date={technicalSkill.date}
            level={technicalSkill.level}
            isHaveWarning={technicalSkill.dueDays > 0}
            days={technicalSkill.dueDays}
            pills={{
              label: technicalSkill.score > 50 ? "Good" : "Poor",
              size: "small",
              color: technicalSkill.score > 50 ? "info" : "error",
            }}
            id={technicalSkill.id}
            onChangeSwitch={onChangeSwitch}
            checked={technicalSkill.isChecked}
          />
        ))}
      </div>

      <div className="flex w-full justify-between items-center mb-6 cursor-pointer">
        {isMobile || isTablet ? (
          <>
            <div className="flex gap-2 items-center" onClick={onDelete}>
              <CustomDeleteIcon fontSize="small" color="primary" />
              <div className="font-bold text-xs text-primary">Remove</div>
            </div>
            <CustomSwitch
              defaultChecked
              color="default"
              onChange={(event) =>
                onCheckAll({ isChecked: event.target.checked })
              }
            />
          </>
        ) : (
          <>
            <CustomSwitch
              defaultChecked
              color="default"
              onChange={(event) =>
                onCheckAll({ isChecked: event.target.checked })
              }
            />
            <div className="flex gap-2 items-center" onClick={onDelete}>
              <CustomDeleteIcon fontSize="small" color="primary" />
              <div className="font-bold text-xs text-primary">Remove</div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default TechnicalSkills;
