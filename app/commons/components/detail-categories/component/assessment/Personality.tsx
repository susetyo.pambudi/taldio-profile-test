import Button from "../../../button";
import InfoIcon from "@mui/icons-material/Info";

const Personality = () => {
  return (
    <div className="mt-4 mx-4">
      <div className="flex w-full justify-between items-center mb-6">
        <div className="text-sm font-bold">Personality</div>
        <Button
          onClick={() => alert("edit item")}
          text="Take Another Test"
          size="medium"
          customClass="px-2"
        />
      </div>
      <div className="flex w-full justify-between items-center mb-6">
        <div>
          <div className="text-sm font-bold flex gap-2 items-center">
            <div>DISC</div>
            <InfoIcon color="primary" />
          </div>
          <div className="text-secondary font-bold text-xs mt-2">
            Last Taken {new Date().toDateString()}
          </div>
          <div className="text-primary font-bold text-xs mt-3">
            View Test History
          </div>
        </div>
        <Button
          onClick={() => alert("edit item")}
          text="View Result"
          size="medium"
          customClass="px-2"
        />
      </div>
    </div>
  );
};

export default Personality;
