import Pills from "../../../pills";
import { styled } from "@mui/material/styles";
import Chip from "@mui/material/Chip";
import { TTechnicalSkillItem } from "../types";
import CustomSwitch from "../../../switch";
import useScreenDetector from "@/app/commons/hooks/useScreenDetector";

const CustomPills = styled(Chip)(() => ({
  "&.MuiChip-colorInfo": {
    backgroundColor: "#ebfaeb",
    borderColor: "#83bf74",
    borderWidth: "1.5px",
    borderStyle: "solid",
    color: "#83bf74",
    width: "100%",
    fontSize: "12px",
    fontWeight: 800,
  },
  "&.MuiChip-colorError": {
    backgroundColor: "#fae6e1",
    borderColor: "#eb3223",
    borderWidth: "1.5px",
    borderStyle: "solid",
    color: "#eb3223",
    width: "100%",
    fontSize: "12px",
    fontWeight: 800,
  },
}));

const TechnicalSkillItem = ({
  skills,
  level,
  date,
  score,
  isHaveWarning,
  days,
  pills,
  id,
  onChangeSwitch,
  checked,
}: TTechnicalSkillItem) => {
  const { isMobile, isTablet } = useScreenDetector();

  if (isMobile || isTablet) {
    return (
      <div className="border border-b-2 border-[#ebebeb] pb-4 border-t-0 border-l-0 border-r-0 mb-4 mr-2">
        <div className="w-full">
          <div className="flex gap-2 items-center justify-between mb-2">
            <div className="">
              <div className="flex gap-2">
                <div className="text-sm font-bold">{skills}</div>
                <Pills color="from-[#d4a237] to-[#44413b]" text={level} />
              </div>
              <div className="text-secondary font-bold text-xs mt-2">
                Last Taken {date.toDateString()}
              </div>
              <div className="text-primary font-bold text-xs mt-3">
                View Assessment History
              </div>
            </div>
            <CustomSwitch
              onChange={(event) =>
                onChangeSwitch({ id, isChecked: event.target.checked })
              }
              defaultChecked
              color="default"
              checked={checked}
              value={checked}
            />
          </div>
          <div className="border-primary border rounded-md p-2 bg-[#faf0dc]">
            <div className="flex gap-2 justify-between items-center">
              <div>Score</div>
              <div className="text-primary font-bold text-xl">{score}%</div>
              <CustomPills
                style={{ width: 150 }}
                label={pills.label}
                size={pills.size}
                color={pills.color}
              />
            </div>
          </div>
          {isHaveWarning && (
            <div className="text-sm mt-2 text-secondary">
              <span className="font-bold">{days} days</span> remaining to retake
              this test
            </div>
          )}
        </div>
      </div>
    );
  }

  return (
    <div className="flex justify-between border border-b-2 border-[#ebebeb] pb-4 border-t-0 border-l-0 border-r-0 mb-4 mr-2">
      <div className="flex gap-2 items-center">
        <CustomSwitch
          onChange={(event) =>
            onChangeSwitch({ id, isChecked: event.target.checked })
          }
          defaultChecked
          color="default"
          checked={checked}
          value={checked}
        />
        <div className="">
          <div className="flex gap-2">
            <div className="text-sm font-bold">{skills}</div>
            <Pills color="from-[#d4a237] to-[#44413b]" text={level} />
          </div>
          <div className="text-secondary font-bold text-xs mt-2">
            Last Taken {date.toDateString()}
          </div>
          <div className="text-primary font-bold text-xs mt-3">
            View Assessment History
          </div>
        </div>
      </div>
      <div className="flex flex-col items-end">
        <div className="border-primary border rounded-md p-2 bg-[#faf0dc]">
          <div className="flex gap-2 mb-2 items-center">
            <div>Score</div>
            <div className="text-primary font-bold text-xl">{score}%</div>
          </div>
          <CustomPills
            label={pills.label}
            size={pills.size}
            color={pills.color}
          />
        </div>
        {isHaveWarning && (
          <div className="text-sm mt-2 text-secondary">
            <span className="font-bold">{days} days</span> remaining to retake
            this test
          </div>
        )}
      </div>
    </div>
  );
};

export default TechnicalSkillItem;
