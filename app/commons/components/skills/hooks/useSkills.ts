"use client";

import { useState, useMemo } from "react";

type TData = {
  skills: string[]
}

const useSkills = ({skills}:TData) => {
  const [isShowMore, setIsShowmore] = useState(skills.length >  5);
  const skillsData = useMemo(() => {
    if(isShowMore) return skills.slice(0, 5);
    return skills
  },[isShowMore, skills])
  const totalHiddenNumber = skills.length - 5;

  const onClickViewAll = () => setIsShowmore(!isShowMore);

  const randomColor = {
    2:"from-[#b52daa] to-[#40313f]",
    3:"from-[#d4a237] to-[#44413b]", 
    4:"from-[#64baaa] to-[#082f64]"
  }

  const functionGenerateRandomColor = (number:number) => {
    if(number % 2 === 0) return 2;
    if(number % 3 === 0) return 3;
    return 4
  }


  return {
    isShowMore,
    setIsShowmore,
    totalHiddenNumber,
    onClickViewAll,
    skillsData,
    randomColor,
    functionGenerateRandomColor
  }

}

export default useSkills