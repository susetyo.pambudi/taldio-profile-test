"use client";
import Button from "@/app/commons/components/button";
import useSkills from "./hooks/useSkills";
import Card from "../card";
import Pills from "../pills";

const Skills = () => {
  const skills = [
    "javascript",
    "php",
    "react",
    "redux",
    "node",
    "laravel",
    "javascript",
    "php",
    "react",
    "redux",
    "node",
    "laravel",
    "javascript",
    "php",
    "react",
    "redux",
    "node",
    "laravel",
  ];
  const {
    isShowMore,
    totalHiddenNumber,
    onClickViewAll,
    skillsData,
    randomColor,
    functionGenerateRandomColor,
  } = useSkills({ skills });

  return (
    <Card customClass="w-full lg:w-[270px] p-4">
      <h2 className="card-title mb-2 font-bold">Skills</h2>
      <div className="flex flex-wrap gap-2 mb-4">
        {skillsData?.map((skill, index) => (
          <Pills
            key={`${skill}-${index}`}
            color={randomColor[`${functionGenerateRandomColor(index)}`]}
            text={skill}
          />
        ))}
      </div>
      {isShowMore && (
        <div className="flex justify-center mb-4 text-xs text-gray-500">
          and {totalHiddenNumber} more.
        </div>
      )}
      <Button
        onClick={onClickViewAll}
        text={isShowMore ? "View All" : "Hide"}
        size="medium"
        customClass="w-full"
      />
    </Card>
  );
};

export default Skills;
