"use client";
import Image from "next/image";
import Text from "../text";
import { AiFillEdit } from "react-icons/ai";
import Card from "../card";
import { US } from "country-flag-icons/react/3x2";
import useScreenDetector from "@/app/commons/hooks/useScreenDetector";

const Profile = () => {
  const { isMobile } = useScreenDetector();

  return (
    <Card customClass="p-4 border-[#e6e6e6] border-2">
      {isMobile ? (
        <div className="">
          <div className="flex justify-end mb-2">
            <div className="cursor-pointer">
              <div className="flex gap-2 items-center">
                <AiFillEdit color="#d76437" size={20} />
                <div className="text-[#d76437] font-bold">Edit</div>
              </div>
            </div>
          </div>
          <div className="flex">
            <div className="rounded-full w-[70px] h-[70px] relative">
              <div className="w-full">
                <Image
                  className="rounded-full"
                  src="https://media.themoviedb.org/t/p/w600_and_h900_bestv2/lldeQ91GwIVff43JBrpdbAAeYWj.jpg"
                  alt="jason-sthatam"
                  fill
                  objectFit="cover"
                />
              </div>
            </div>
            <div className="ml-4">
              <div className="mb-2 text-xl font-extrabold flex gap-2 items-center">
                <div>Rifki Akbar Siregar</div>
                <US title="United States" className="w-[20px] h-[20px]" />
              </div>
              <div className="text-base text-[#888888]">Product Designer</div>
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-between">
          <div className="flex">
            <div className="rounded-full w-[70px] h-[70px] relative">
              <div className="w-full">
                <Image
                  className="rounded-full"
                  src="https://media.themoviedb.org/t/p/w600_and_h900_bestv2/lldeQ91GwIVff43JBrpdbAAeYWj.jpg"
                  alt="jason-sthatam"
                  fill
                  objectFit="cover"
                />
              </div>
            </div>
            <div className="ml-4">
              <div className="mb-2 text-xl font-extrabold flex gap-2 items-center">
                <div>Rifki Akbar Siregar</div>
                <US title="United States" className="w-[20px] h-[20px]" />
              </div>
              <div className="text-base text-[#888888]">Product Designer</div>
            </div>
          </div>
          <div className="cursor-pointer">
            <div className="flex gap-2 items-center">
              <AiFillEdit color="#d76437" size={20} />
              <div className="text-[#d76437] font-bold">Edit</div>
            </div>
          </div>
        </div>
      )}

      <div
        className={isMobile ? "mt-4" : "flex justify-between mt-4 flex-wrap"}
      >
        <Text
          customClass={isMobile ? "mt-2" : ""}
          label="Phone Number"
          text="(+62) 082357862032"
        />
        <Text
          customClass={isMobile ? "mt-2" : ""}
          label="Email"
          text="rifki@mail.com"
        />
        <Text customClass={isMobile ? "mt-2" : ""} label="Gender" text="Male" />
        <Text
          customClass={isMobile ? "mt-2" : ""}
          label="Religion"
          text="Islam"
        />
        <Text
          customClass={isMobile ? "mt-2" : ""}
          label="Place and Date of Birth"
          text="Islam"
        />
      </div>
      <Text
        customClass="mt-4"
        label="Address"
        text="Jl. Tegal Parang Selatan, Mampang Prapatan, Jakarta Selatan, DKI Jakarta"
      />
      <Text
        customClass="mt-4"
        label="About Me"
        text="Driven Front-End Engineer (7+ years) with expert React.js skills. Led 5 junior engineers, tackled complex challenges, and built key features like a consolidated truck tracking system. Passionate about innovation and growth."
      />
    </Card>
  );
};

export default Profile;
