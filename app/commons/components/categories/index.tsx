"use client";
import useCategories from "./hooks/useCategories";
import Card from "../card";
import useScreenDetector from "../../../commons/hooks/useScreenDetector";

const Categories = () => {
  const { isMobile, isTablet } = useScreenDetector();
  const { categories, onClick } = useCategories();

  return (
    <Card
      customClass={`${
        isMobile || isTablet ? "w-full mb-2" : "w-80 max-h-[410px]"
      } p-4 `}
    >
      {categories?.map((category, index) => (
        <div
          onClick={() => onClick(index)}
          key={index}
          className={`flex gap-2 items-center mb-6 cursor-pointer ${
            category.isActive
              ? "text-[#d76437] font-bold"
              : "hover:text-[#d76437] font-medium"
          }`}
        >
          {category?.Icon}
          <div className="text-sm ">{category?.label}</div>
        </div>
      ))}
    </Card>
  );
};

export default Categories;
