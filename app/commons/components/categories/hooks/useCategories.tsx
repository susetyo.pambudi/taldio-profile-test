"use client";
import { useDispatch, useSelector } from "react-redux";
import { clickMenuSidebar } from "@/lib/features/categories/categoriesSlice";
import { AppDispatch, RootState } from "@/lib/store";

const useCategories = () => {
  const categories = useSelector((state: RootState) => state.categories).data;
  const dispatch = useDispatch<AppDispatch>();

  const onClick = (index: number) => {
    const newCategories = categories.map((item, i) => {
      return {
        ...item,
        isActive: i === index,
      };
    });

    dispatch(clickMenuSidebar(newCategories));
  };

  return {
    categories,
    onClick,
  };
};

export default useCategories;
