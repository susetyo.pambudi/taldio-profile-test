import { Card as CardMaterialUi, CardContent } from "@mui/material";
import { styled } from "@mui/material/styles";
import { ReactNode } from "react";

type TCard = {
  customClass?: string;
  children: ReactNode;
};

const CustomCard = styled(CardMaterialUi)(() => ({
  "&.MuiCard-root": {
    borderRadius: "15px",
    boxShadow:
      "0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1)",
  },
}));

const CustomCardContent = styled(CardContent)(() => ({
  "&.MuiCardContent-root": {
    padding: 0,
  },
}));

const Card = ({ children, customClass }: TCard) => {
  return (
    <CustomCard className={customClass ? customClass : "w-full"}>
      <CustomCardContent>{children}</CustomCardContent>
    </CustomCard>
  );
};

export default Card;
