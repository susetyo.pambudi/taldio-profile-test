"use client";
import { useEffect, useState } from "react";

const useScreenDetector = () => {
  const [width, setWidth] = useState(0);

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);

    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  const isMobile = width <= 768 && width !== 0;
  const isTablet = width <= 1024 && width !== 0;
  const isDesktop = width > 1024 && width !== 0;

  return { isMobile, isTablet, isDesktop, width };
};

export default useScreenDetector;
