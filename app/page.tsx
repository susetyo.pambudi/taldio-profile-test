"use client";
import Skills from "@/app/commons/components/skills";
import CompletionProgress from "@/app/commons/components/completion-progress";
import Profile from "@/app/commons/components/profile";
import Categories from "@/app/commons/components/categories";
import DetailCategories from "@/app/commons/components/detail-categories";
import Button from "./commons/components/button";
import useScreenDetector from "./commons/hooks/useScreenDetector";

export default function Home() {
  const { isMobile, isTablet, isDesktop, width } = useScreenDetector();

  if (width === 0) {
    return (
      <main className="h-screen p-4 justify-center items-center text-4xl flex">
        Loading....
      </main>
    );
  }

  return (
    <main className="min-h-screen p-4">
      <div className="flex justify-between mb-6 items-center">
        <div className="font-bold text-3xl">Profile</div>
        <div>
          <Button text="Generate CV" size="large" />
        </div>
      </div>

      {(isMobile || isTablet) && (
        <>
          <Profile />
          <div className="mb-2" />
          <CompletionProgress />
          <div className="mb-2" />
          <Skills />
          <div className="mb-2" />
          <Categories />
          <div className="mb-2" />
          <DetailCategories />
        </>
      )}

      {isDesktop && (
        <>
          <div className="flex gap-4">
            <Profile />
            <div>
              <CompletionProgress />
              <Skills />
            </div>
          </div>
          <div className="flex gap-4 mt-2">
            <Categories />
            <DetailCategories />
          </div>
        </>
      )}
    </main>
  );
}
