## Getting Started

First, install dependecy on your local:

```bash
npm install
```

Second, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## You can see the result on this url:

[https://main--taldio-profile.netlify.app/](https://main--taldio-profile.netlify.app/)

## You can find the screenshoot image on:

```bash
root[folder] > screenshoot[folder]
```
